import React from "react";
import { Progress } from "semantic-ui-react";

const ProgressBar = ({ uploadState, percentUploaded }) => (
    uploadState === "uploading" && (
        <Progress 
            progress
            className="progress__bar"
            percent={percentUploaded}
            indicating
            size="medium"
            inverted
        />
    )
);

export default ProgressBar;
